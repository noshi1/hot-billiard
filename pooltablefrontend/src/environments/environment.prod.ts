export const environment = {
  production: true,
  webSocketApi: `ws://${window.location.hostname}:8080/ws`,
  restApi: `http://${window.location.hostname}:8080`
};
