import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { MainMenuComponent } from './components/menu/main/main-menu.component';
import { OptionsComponent } from './components/menu/options/options.component';
import { NewGameComponent } from './components/menu/new-game/new-game.component';
import { ChallengeLevelComponent } from './components/menu/new-game/challenge/challenge-level/challenge-level.component';
import { ChallengeDifficultyComponent } from './components/menu/new-game/challenge/challenge-difficulty/challenge-difficulty.component';
import { PoolPageComponent } from './components/pool-page/pool-page.component';
import { PoolDisplayComponent } from './components/pool-display/pool-display.component';
import { ImgLiveComponent } from './components/img-live/img-live.component';
import { PoolEditorComponent } from './components/pool-editor/pool-editor.component';
import { LogComponent } from './components/log/log.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { TrainingModeParamsComponent } from './components/menu/options/training-mode-params/training-mode-params.component';
import { PoolDrawerParamsComponent } from './components/menu/options/pool-drawer-params/pool-drawer-params.component';
import { CalibrationParamsComponent } from './components/menu/options/calibration-params/calibration-params.component';
import { ViewModeParamsComponent } from './components/menu/options/view-mode-params/view-mode-params.component';


@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    OptionsComponent,
    NewGameComponent,
    ChallengeLevelComponent,
    ChallengeDifficultyComponent,
    PoolPageComponent,
    PoolDisplayComponent,
    ImgLiveComponent,
    PoolEditorComponent,
    LogComponent,
    TrainingModeParamsComponent,
    PoolDrawerParamsComponent,
    CalibrationParamsComponent,
    ViewModeParamsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
