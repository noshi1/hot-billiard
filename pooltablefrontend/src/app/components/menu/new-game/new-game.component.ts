import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common'
import { TrainingService } from 'src/app/services/training.service';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent implements OnInit {

  constructor(private router: Router, private location: Location,
              private trainingService: TrainingService) { }

  ngOnInit() {
  }

  startGame() {
    this.trainingService.setChallenge(0).then(_ => {
      this.router.navigate(['/poolTable']);
    })
  }

  navigateToChallengePage() {
    this.router.navigate(['/challenge/difficulty']);
  }

  navigateToGeneratorPage() {
    this.router.navigate(['/poolEditor']);
  }

  navigateToPreviousPage() {
    this.location.back();
  }
}
