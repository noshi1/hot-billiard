import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { Router } from '@angular/router';
import { DifficultyLevel } from 'src/app/models/difficulty-level';

@Component({
  selector: 'app-challenge-difficulty',
  templateUrl: './challenge-difficulty.component.html',
  styleUrls: ['./challenge-difficulty.component.css']
})
export class ChallengeDifficultyComponent implements OnInit {

  constructor(private router: Router, private location: Location) {
  }

  ngOnInit() {
  }
  
  selectDifficultyLevel(difficultyLevel: string) {
    this.router.navigate(['/challenge/level', DifficultyLevel[difficultyLevel]]);
  }

  navigateToPreviousPage() {
    this.location.back();
  }

}
