import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DifficultyLevel } from 'src/app/models/difficulty-level';
import { TrainingService } from 'src/app/services/training.service';
import { Training } from 'src/app/models/training/training';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-challenge-level',
  templateUrl: './challenge-level.component.html',
  styleUrls: ['./challenge-level.component.css']
})
export class ChallengeLevelComponent implements OnInit {

  difficultyLevel: DifficultyLevel = DifficultyLevel.Normal;
  trainingsIds: number[] = [];

  trainings: Training[] = [];

  currentPage: number = 1;
  itemsPerPage: number = 12;
  itemsOnCurrentPage: number = 2;
  totalItems: number = 10;

  constructor(private router: Router, private location: Location,
              private route: ActivatedRoute,
              private trainingService: TrainingService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.difficultyLevel = params.get('difficultyLevel') as DifficultyLevel;
      this.getPage(1);
    });
  }

  openModal(content, trainingId) {
    this.modalService.open(content).result.then(() => {
        this.trainingService.deleteTrainingById(trainingId).then(() => {
          if(this.itemsOnCurrentPage == 1 && this.currentPage > 1) {
            this.currentPage--;
          }
          this.getPage(this.currentPage);
        });
    }, () => {});
  }

  getPage(page: number) {
    this.trainingService.fetchTrainingsByDifficultyLevel(this.difficultyLevel, page - 1, this.itemsPerPage)
      .then(response => {
        this.trainings = response['content'] as Training[];
        this.currentPage = page;
        this.itemsOnCurrentPage = response['numberOfElements'];
        this.totalItems = response['totalElements'];
      });
  }

  getDifficultyString() {
    if(this.difficultyLevel == DifficultyLevel.Easy) {
      return "Łatwy";
    } else if(this.difficultyLevel == DifficultyLevel.Normal) {
      return "Normalny";
    } else /* if(this.difficulty == DifficultyLevel.Hard) */{
      return "Trudny";
    }
  }

  selectLevel(level: number) {
    this.trainingService.setChallenge(level);
    this.router.navigate(['/poolTable']);
  }

  editLevel(level: number) {
    this.router.navigate(['/poolEditor', level]);
  }
  
  navigateToPreviousPage() {
    this.location.back();
  }
}
