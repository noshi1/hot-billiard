import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent {

  constructor(private router: Router) { }

  navigateToNewGame() {
    this.router.navigate(['/newGame'])
  }

  navigateToOptions() {
    this.router.navigate(['/options'])
  }

  navigateToDisplay() {
    this.router.navigate(['/imglive'])
  }
}

