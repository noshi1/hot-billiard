import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { PoolDrawerParams } from 'src/app/models/pool-drawer-params';
import { PoolDrawerParamsService } from 'src/app/services/pool-drawer-params.service';

@Component({
  selector: 'app-pool-drawer-params',
  templateUrl: './pool-drawer-params.component.html',
  styleUrls: ['./pool-drawer-params.component.css']
})
export class PoolDrawerParamsComponent implements OnInit {

  poolDrawerParams: PoolDrawerParams = new PoolDrawerParams();

  constructor(private location: Location, 
              private poolDrawerParamsService: PoolDrawerParamsService) { }

  ngOnInit() {
    this.poolDrawerParamsService.fetch()
      .then(poolDrawerParams => this.poolDrawerParams = poolDrawerParams)
  }

    
  updatePoolDrawerParams() {
    this.poolDrawerParamsService.update(this.poolDrawerParams)
      .then(poolDrawerParams => this.poolDrawerParams = poolDrawerParams);
  }

  navigateToPreviousPage() {
    this.location.back();
  }
}
