import { Component, OnInit } from '@angular/core';
import { TrainingModeParams } from 'src/app/models/training/training-mode-params';
import { TrainingService } from 'src/app/services/training.service';
import { Location } from '@angular/common';
import { AfterEndAction } from 'src/app/models/training/after-end-action.enum';

@Component({
  selector: 'app-training-mode-params',
  templateUrl: './training-mode-params.component.html',
  styleUrls: ['./training-mode-params.component.css']
})
export class TrainingModeParamsComponent implements OnInit {

  AfterEndAction = AfterEndAction;
  trainingModeParams: TrainingModeParams = new TrainingModeParams();

  constructor(private location: Location,
              private trainingService: TrainingService) { }

  ngOnInit() {
    this.trainingService.fetchTrainingModeParams()
      .then(trainingModeParams => this.trainingModeParams = trainingModeParams);
  }

  
  updateTrainingModeParams() {
    this.trainingService.updateTrainingModeParams(this.trainingModeParams)
      .then(trainingModeParams => this.trainingModeParams = trainingModeParams);
  }

  navigateToPreviousPage() {
    this.location.back();
  }
}
