import { Component, OnInit } from '@angular/core';
import { CalibrationService } from 'src/app/services/calibration.service';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { CalibrationParams } from 'src/app/models/calibration-params';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-calibration-params',
  templateUrl: './calibration-params.component.html',
  styleUrls: ['./calibration-params.component.css']
})
export class CalibrationParamsComponent implements OnInit {

  calibrationParams: CalibrationParams = new CalibrationParams(); 

  private unsubscribtionSubject: Subject<void> = new Subject();

  constructor(private location: Location,
              private calibrationService: CalibrationService) { }

  ngOnInit() {
    this.calibrationService
      .fetchCalibration()
      .then(calibration => {
        this.calibrationParams = calibration;

        this.calibrationService
          .fetchCalibrationLive()
          .pipe(takeUntil(this.unsubscribtionSubject))
          .subscribe(calibration => this.calibrationParams = calibration);
      });
  }
  

  ngOnDestroy(): void {
    this.unsubscribtionSubject.next();
    this.unsubscribtionSubject.complete();
  }

  updateCalibration() {
    this.calibrationService.updateCalibration(this.calibrationParams);
  }

  autoCalibration() {
    this.calibrationService.autoCalibration();
  }

  resetToDefault() {
    this.calibrationService.resetToDefault().then(calibration => this.calibrationParams = calibration);
  }

  navigateToPreviousPage() {
    this.location.back();
  }

}
