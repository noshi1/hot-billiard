import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css']
})
export class OptionsComponent {

  constructor(private location: Location) { }

  navigateToPreviousPage() {
    this.location.back();
  }
}
