import { Component, OnInit } from '@angular/core';
import { ViewModeParams } from 'src/app/models/view-mode-params';
import { Location } from '@angular/common';
import { PassiveModeLevel } from 'src/app/models/passive-mode-level';

@Component({
  selector: 'app-view-mode-params',
  templateUrl: './view-mode-params.component.html',
  styleUrls: ['./view-mode-params.component.css']
})
export class ViewModeParamsComponent {

  PassiveModeLevel = PassiveModeLevel;

  // temporally, later will use viewMode and viewModeFeatures
  viewModeParams: ViewModeParams = new ViewModeParams;

  constructor(private location: Location) { }

  navigateToPreviousPage() {
    this.location.back();
  }
}
