import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgLiveComponent } from './img-live.component';

describe('ImgLiveComponent', () => {
  let component: ImgLiveComponent;
  let fixture: ComponentFixture<ImgLiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImgLiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgLiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
