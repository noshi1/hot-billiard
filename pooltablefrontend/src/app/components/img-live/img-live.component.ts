import { TableService } from 'src/app/services/table.service';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-img-live',
  templateUrl: './img-live.component.html',
  styleUrls: ['./img-live.component.css']
})
export class ImgLiveComponent implements OnInit {

  private unsubscribeSubject: Subject<void> = new Subject()
  url: string;
  
  


  constructor(private tableService: TableService) { }


  ngOnInit() {
    console.log("on init.");
    //this.fetchTable();
    this.tableService
    .drawTableLive()
    .pipe(takeUntil(this.unsubscribeSubject))
    .subscribe(image => {
      console.log("start");
      //console.clear();



      //console.log("image.content : ")
      //console.log(image);


      var base64 = image;
      this.url = 'data:image/png;base64,' + base64;


      // console.log("bytes : ");
      // console.log(bytes);

      // var uints = new Uint8Array(bytes);
      // console.log("uints");
      // console.log(uints)
      // var base64 = btoa(String.fromCharCode(null, uints));
      // console.log("base64");
      // console.log(base64);
      //this.url = 'data:image/jpg;base64,' + base64; // use this in <img src="..."> binding

      


    });
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

}
