import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoolDisplayComponent } from './pool-display.component';

describe('PoolDisplayComponent', () => {
  let component: PoolDisplayComponent;
  let fixture: ComponentFixture<PoolDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoolDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoolDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
