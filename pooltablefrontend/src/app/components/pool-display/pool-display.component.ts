import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common'
import { TableService } from 'src/app/services/table.service';
import { Table } from 'src/app/models/table';
import { Point } from 'src/app/models/point';
import { Pocket } from 'src/app/models/pocket';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Ball } from 'src/app/models/ball';
import { Training } from 'src/app/models/training/training';
import { TrainingService } from 'src/app/services/training.service';


@Component({
  selector: 'app-pool-display',
  templateUrl: './pool-display.component.html',
  styleUrls: ['./pool-display.component.css']
})
export class PoolDisplayComponent implements OnInit, OnDestroy {
  

  private context: CanvasRenderingContext2D;
  @ViewChild("mycanvas",{static:false}) mycanvas;
  @ViewChild('canvas', { static: true }) 
  private canvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;

  
  private kinectPlayZoneHeight: number = 620; //number; // odczytac od kinecta
  private kinectPlayZoneWidth: number = 1190; //number; // odczytac od kinecta

  // " - 7" zeby nie pokazywal sie pasek przesuwania zawartosci strony.
  private displayPlayZoneHeight: number = 1080 - 7 - 210;//number; // zapisac wartosc domyslna do pliku
  private displayPlayZoneWidth: number = 1920 - 190 ;//number; // zapisac wartosc domyslna do pliku

  // 0 - zaznaczanie bil, 
  // 1 - pojedyncza sciezka,
  // zaznaczenia, 3- wszystko
  private currentViewMode: number = 0; 

  // wspolczynnik wielkosci * domyslna wielkosc//13; // srednica wyswietlanej bili
  private diameter: number = (this.displayPlayZoneHeight / this.kinectPlayZoneHeight) * 8; 
  private pocketDiameter: number = 8;
  private hitPoints: Point[];
  private allPossibleHits: Point[];

  // // utworzone zeby zasymulowac backend
  // aimPockets: Pocket[] = [
  //   new Pocket(1,new Point(0, 0)),
  //   new Pocket(2,new Point(this.displayPlayZoneWidth/2, 0)),
  //   new Pocket(3,new Point(this.displayPlayZoneWidth, 0)),
  //   new Pocket(4,new Point(0, this.displayPlayZoneHeight)),
  //   new Pocket(5,new Point(this.displayPlayZoneWidth/2,this.displayPlayZoneHeight)),
  //   new Pocket(6,new Point(this.displayPlayZoneWidth,this.displayPlayZoneHeight))
  // ];

  // utworzone zeby zasymulowac backend
  // balls: Ball[] = [
  //   new Ball(0, new Point(1000,800)),
	// 	new Ball(1, new Point(55,222)),
	// 	new Ball(2, new Point(400,29)),
	// 	new Ball(3, new Point(111,99)),
	// 	new Ball(4, new Point(417,29)),
	// 	new Ball(6, new Point(800,450))
  // ];

  private unsubscribeSubject: Subject<void> = new Subject();

  private table: Table;
  private training: Training;
  // utworzone zeby zasymulowac backend
  // private table: Table = new Table(
  //   this.kinectPlayZoneHeight,
  //   this.kinectPlayZoneWidth,
  //   this.balls,
  //   this.aimPockets,
  //   this.balls[1],
  //   this.balls[2],
  //   this.aimPockets[2],
  //   this.hittingPoint,
  //   this.allPossibleHits
  // );


  constructor(private tableService: TableService,
              private trainingService: TrainingService) { }


  
  private c = document.getElementById("canvas");

  ngOnInit() {

    //this.fetchTable();

    this.tableService
    .fetchTableLive()
    .pipe(takeUntil(this.unsubscribeSubject))
    .subscribe(table => {
      //console.clear();
      
      
      this.table = table;
      //console.log(this.table);


      if(this.table.balls != null) {
        for ( let ball of this.table.balls ) {
          ball.point = this.scalePosToView(ball.point);
        }
      }

      if(this.table.pockets != null) {
        for ( let pocket of this.table.pockets ){
          pocket.point = this.scalePosToView(pocket.point);
        }
      }

      if( this.table.whiteBall != null)
        this.table.whiteBall.point = this.scalePosToView(this.table.whiteBall.point);
      if( this.table.selectedBall != null)
        this.table.selectedBall.point = this.scalePosToView(this.table.selectedBall.point);
      if( this.table.selectedPocket != null)
        this.table.selectedPocket.point = this.scalePosToView(this.table.selectedPocket.point);
      

      //console.log(this.table);
      if(this.table.selectedChallenge == 0){
        // rysuje tryby tylko jesli nie ma zaznaczonego challenge'u
        switch(this.table.selectedViewMode){
          case 0: this.drawViewMode0(); break;
          case 1: this.drawViewMode1(); break;
          //case 2: this.drawViewMode2(); break;
        }
      } else{
        // tryb challenge'u zostal wybrany.
        this.fetchTrainingById(this.table.selectedChallenge);
      }
    });
    
    //this.fetchTable();
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.canvas.nativeElement.height = this.displayPlayZoneHeight;
    this.canvas.nativeElement.width = this.displayPlayZoneWidth;
  }



  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }


  fetchTable(){
    this.tableService.fetchTable().then ( response => {
      console.clear();
      console.log(response);
      console.log(response.selectedViewMode);
      this.table = response;


      if(this.table.balls != null) {
        for ( let ball of this.table.balls ) {
          ball.point = this.scalePosToView(ball.point);
        }
      }

      if(this.table.pockets != null) {
        for ( let pocket of this.table.pockets ){
          pocket.point = this.scalePosToView(pocket.point);
        }
      }

      if( this.table.whiteBall != null)
        this.table.whiteBall.point = this.scalePosToView(this.table.whiteBall.point);
      if( this.table.selectedBall != null)
        this.table.selectedBall.point = this.scalePosToView(this.table.selectedBall.point);
      if( this.table.selectedPocket != null)
        this.table.selectedPocket.point = this.scalePosToView(this.table.selectedPocket.point);
      

      //console.log(this.table);
      if(this.table.selectedChallenge == 0){
        // rysuje tryby tylko jesli nie ma zaznaczonego challenge'u
        switch(this.table.selectedViewMode){
          case 0: this.drawViewMode0(); break;
          case 1: this.drawViewMode1(); break;
          //case 2: this.drawViewMode2(); break;
        }
      } else{
        // tryb challenge'u zostal wybrany.
        this.fetchTrainingById(this.table.selectedChallenge);
      }
      //this.drawPockets();
      // this.drawBalls();
      // this.drawPockets();
      // this.fetchHitPoint();
      // this.fetchHints();
    }).catch(err => {
      console.log(err);
    });
  }


  // fetchTable(){
  //   this.tableService.fetchTable().then ( response => {
  //     //console.log(response);
  //     this.table = response;
  //     for ( let ball of this.table.balls ){
  //       ball.point = this.scalePosToView(ball.point);
  //     }

      
  //     switch(this.currentViewMode){
  //       case 0: this.drawViewMode0(); break;
  //       case 1: this.drawViewMode1(); break;
  //       //case 3: this.drawViewMode2(); break;
  //     }
      
  //   }).catch(err => {
  //     console.log(err);
  //   });
  // }

  drawViewMode0(){
    this.ctx.clearRect(0,0,window.innerWidth,window.innerHeight);
    this.drawBalls();
    this.drawPockets();
  }

  drawViewMode1(){
    this.ctx.clearRect(0,0,window.innerWidth,window.innerHeight);
    this.drawSelected();
    //this.drawPockets();
    //this.fetchTable();
    //console.log("view mode 1 - trajektoria");
    
    if((this.table.selectedBall != null) && (this.table.selectedPocket != null)){
      this.fetchHittingPoint();
        //console.log(this.hittingPoint);
        //this.drawTrajectory(this.hittingPoint);
    }else{
      //this.drawBalls();
    }
  }


  drawTraining(){
    this.ctx.clearRect(0,0,window.innerWidth,window.innerHeight);

    //console.log(this.training);

    // rysowanie punktu ustawienia bialej bili
    this.ctx.beginPath();
    this.ctx.arc(
      this.training.whiteBallPosition.x,
      this.training.whiteBallPosition.y,
      this.diameter,
      0,
      2 * Math.PI
    );
    this.ctx.fillStyle = "#FFF";
    this.ctx.lineWidth = 3;
    this.ctx.fill();


    // rysowanie punktu ustawienia zaznaczonej bili
    this.ctx.beginPath();
    this.ctx.arc(
      this.training.selectedBallPosition.x,
      this.training.selectedBallPosition.y,
      this.diameter,
      0,
      2 * Math.PI
    );
    this.ctx.fillStyle = "#FF0";
    this.ctx.lineWidth = 3;
    this.ctx.fill();


    // rysowanie przeszkadzajek
    for ( let ball in this.training.disturbBallsPositions ){
      this.ctx.beginPath();
      this.ctx.arc(
        this.training.disturbBallsPositions[ball].x,
        this.training.disturbBallsPositions[ball].y,
        this.diameter,
        0,
        2 * Math.PI
      );
      this.ctx.fillStyle = "#F00";
      this.ctx.lineWidth = 3;
      this.ctx.fill();
    }

    //rysowanie prostokatu w ktorym ma sie zatrzymac biala
    this.ctx.beginPath();
    this.ctx.rect(
      this.training.rectanglePosition[0].x,
      this.training.rectanglePosition[0].y,
      this.training.rectanglePosition[1].x - this.training.rectanglePosition[0].x,
      this.training.rectanglePosition[1].y - this.training.rectanglePosition[0].y
    );
    this.ctx.strokeStyle = "#FFF";
    this.ctx.stroke();

    // rysowanie zaznaczenia luzy do ktorej ma wpasc bila
    this.ctx.beginPath();
    this.ctx.arc(
      this.table.pockets[this.training.pocketId].point.x,
      this.table.pockets[this.training.pocketId].point.y,
      70,
      0,
      2 * Math.PI);
    this.ctx.strokeStyle = "#FF0";
    this.ctx.lineWidth = 10;
    this.ctx.stroke();
    this.ctx.lineWidth = 1;
    // this.ctx.beginPath();
    // this.ctx.arc(
    //   this.touchPockets[this.newTraining.idPocket].point.x,
    //   this.touchPockets[this.newTraining.idPocket].point.y,
    //   this.touchPocketDiameter + 5,
    //   0,
    //   2 * Math.PI);
    // this.ctx.strokeStyle = "#F00";
    // this.ctx.lineWidth = 10;
    // this.ctx.stroke();
    // this.ctx.lineWidth = 1;


  } // end of drawTraining();


  drawBalls(){ 
    if ( this.table.balls.length == 0) {
      return;
    }
    
    for ( let ball of this.table.balls){
      // rysowanie bil na stole
      this.ctx.beginPath();
      this.ctx.arc(
        ball.point.x, ball.point.y,
        this.diameter * 3,
        0,
        2 * Math.PI
      );
      this.ctx.strokeStyle = "#0FF";
      this.ctx.lineWidth = 5;

      //

      this.ctx.stroke();


      // podpisywanie bil
      this.ctx.fillStyle = "#FFF";
      // if(this.table.whiteBall.id == ball.id){
      //   this.ctx.fillStyle = "#000";
      // }
      this.ctx.fillText(
        ball.id.toString(),
        ball.point.x - 3,
        ball.point.y + 3
      ); 



      if( this.currentViewMode == 1){
        // rysowanie obramowania zaznaczonej bili
        this.ctx.beginPath();
        this.ctx.arc(
          this.table.selectedBall.point.x,
          this.table.selectedBall.point.y,
          this.diameter + 4,
          0,
          2 * Math.PI
        );
        this.ctx.strokeStyle = "#F00";
        this.ctx.stroke();


        //rysowanie obramowania zaznaczonego pocketu
        this.ctx.beginPath();
        this.ctx.arc(
          this.table.selectedPocket.point.x,
          this.table.selectedPocket.point.y,
          20,
          0,
          2 * Math.PI);
        this.ctx.strokeStyle = "#F00";
        this.ctx.lineWidth = 5;
        this.ctx.stroke();
        this.ctx.lineWidth = 1;
      }

      // // rysowanie zaznaczenia bili
      // if(ball.selected == true){
      //   this.ctx.beginPath();
      //   this.ctx.arc(
      //     ball.point.x,
      //     ball.point.y,
      //     this.diameter + 4,
      //     0,
      //     2 * Math.PI
      //   );
      //   this.ctx.strokeStyle = "#FF0";
      //   this.ctx.stroke();
      // }

    }


    


    // rysowanie wiekszej otoczki dla bialej bili
    if ( this.table.whiteBall != null ){
      this.ctx.beginPath();
      this.ctx.arc(
        this.table.whiteBall.point.x, this.table.whiteBall.point.y,
        this.diameter + 3,
        0,
        2 * Math.PI
      );
      this.ctx.strokeStyle = "#FFF";
      this.ctx.stroke();
    }

  } // end of drawBalls();

  drawSelected(){ 
    if ( this.table.balls.length == 0) {
      return;
    }
    
    // rysowanie bialej bili
    if( this.table.whiteBall != null ){
      this.ctx.beginPath();
      this.ctx.arc(
        this.table.whiteBall.point.x, this.table.whiteBall.point.y,
        this.diameter * 4,
        0,
        2 * Math.PI
      );
      this.ctx.fillStyle = "#FFF";
      this.ctx.lineWidth = 5;
      this.ctx.fill();
    }

    // rysowanie wybranej zaznaczonej bili
    if( this.table.selectedBall != null ){
      this.ctx.beginPath();
      this.ctx.arc(
        this.table.selectedBall.point.x, this.table.selectedBall.point.y,
        this.diameter * 3,
        0,
        2 * Math.PI
      );
      this.ctx.fillStyle = "#FF0";
      this.ctx.lineWidth = 4;
      this.ctx.fill();
    }

    // rysowanie obramowania zaznaczonego pocketu
    if ( this.table.selectedPocket != null) {
      this.ctx.beginPath();
      this.ctx.arc(
        this.table.selectedPocket.point.x,
        this.table.selectedPocket.point.y,
        20,
        0,
        2 * Math.PI);
      this.ctx.strokeStyle = "#F00";
      this.ctx.lineWidth = 5;
      this.ctx.stroke();
      this.ctx.lineWidth = 1;
    }

    // // rysowanie wiekszej otoczki dla bialej bili
    // if ( this.table.whiteBall != null ){
    //   this.ctx.beginPath();
    //   this.ctx.arc(
    //     this.table.whiteBall.point.x, this.table.whiteBall.point.y,
    //     this.diameter + 3,
    //     0,
    //     2 * Math.PI
    //   );
    //   this.ctx.strokeStyle = "#FFF";
    //   this.ctx.stroke();
    // }

  } // end of drawSelected();






  drawPockets(){
    console.log("draw pockets");


    // //rysowanie obszaru "dotkniecie" pocketu
    // for ( let pocket of this.touchPockets){
    //   this.ctx.beginPath();
    //   this.ctx.arc((pocket.point.x),(pocket.point.y),this.touchPocketDiameter,0,2 * Math.PI);
    //   this.ctx.fillStyle = "#FFF";
    //   this.ctx.fill();
    // }

    // //rysowanie powierzchni pocketu na obrazku
    // for ( let pocket of this.aimPockets){
    //   //console.log(pocket);
    //   this.ctx.beginPath();
    //   this.ctx.arc((pocket.point.x),(pocket.point.y),10,0,2 * Math.PI);
    //   this.ctx.fillStyle = "#F00";
    //   this.ctx.fill();
    // }


    // //rysowanie pocketow przeskalowanych pobranych ze stolu
    for ( let pocket of this.table.pockets){
      //console.log(pocket);
      //console.log("rysu rysu");
      this.ctx.beginPath();
      this.ctx.arc(pocket.point.x,pocket.point.y,30,0,2 * Math.PI);
      this.ctx.strokeStyle = "#F00";
      this.ctx.stroke();
    }

  } // end of drawPockets();


  drawTrajectory(hitPoints: Point[]){

    
    if( hitPoints.length == 1 ){
      // jeden punkt oznacza, ze jest prosta droga do łuzy

      // linia od bialej bili do wyznaczonej
      this.ctx.beginPath();
      this.ctx.moveTo(this.table.whiteBall.point.x,
        this.table.whiteBall.point.y
      );
      this.ctx.lineTo(this.hitPoints[0].x,
        this.hitPoints[0].y
      );

      // linia od wyznaczonej bili do łuzy
      this.ctx.lineTo(this.table.selectedPocket.point.x, this.table.selectedPocket.point.y);    
      this.ctx.strokeStyle = "#FF0";
      this.ctx.stroke();
    } 
    else if ( hitPoints.length == 2){
      // dwa punkty oznaczaja, ze najpierw jest odbicie od bandy [1] a pozniej do bili [0].

      // linia od bialej bili
      this.ctx.beginPath();
      this.ctx.moveTo(this.table.whiteBall.point.x,
        this.table.whiteBall.point.y
      );
      // do wyznaczonej bandy
      this.ctx.lineTo(this.hitPoints[1].x,
        this.hitPoints[1].y
      );
      // do wyznaczonej bili
      this.ctx.lineTo(this.hitPoints[0].x,
        this.hitPoints[0].y
      );

      // linia od wyznaczonej bili do łuzy
      this.ctx.lineTo(this.table.selectedPocket.point.x, this.table.selectedPocket.point.y);    
      this.ctx.strokeStyle = "#FF0";
      this.ctx.lineWidth = 4;
      this.ctx.stroke();
    }
    
    // rysowanie okregu bili w miejscu uderzenia
    this.ctx.beginPath();
      this.ctx.arc(
        this.hitPoints[0].x,
        this.hitPoints[0].y,
        this.diameter,
        0,
        2 * Math.PI
      );
      this.ctx.strokeStyle = "#000";
      this.ctx.stroke();
  }



  fetchHittingPoint(){
    this.tableService.fetchHittingPoint().then ( response => {
      // reset pobranych punktow
      this.hitPoints = [];

      if(response[0] === undefined){
        // nie zostaly wrocone w odpowiedzi zadne wspolrzedne. blad?
        console.log("fetchHittingPoint(); response[0] is unfefined");
        return;
      } 
      else {
        // w odpowiedzi zostal poday pierwszy punkt
        response[0] = this.scalePosToView(response[0]);
        this.hitPoints.push(
          new Point(response[0].x, response[0].y)
        );
      }
      
      if(response[1] !== undefined){
        // drugi punkt wspolrzednych istnieje.
        response[1] = this.scalePosToView(response[1]);
        this.hitPoints.push(
          new Point(response[1].x, response[1].y)
        );
      }
    
      this.drawTrajectory(this.hitPoints);
    }).catch(err => {
      this.drawSelected();
    });
  }

  fetchTrainingById(id: number){
    this.trainingService.fetchTrainingInPixelById(id).then ( training => {
      this.training = training;
      //console.log(this.newTraining);
      this.drawTraining();
    
    }).catch(err => {
      console.log(err);
    });
  }


    // // przyjmuje pozycje x,y z kinecta i skaluje na pozycje do wyswietlenia
    // scalePosToView(point: Point){
    //   return new Point(
    //     (point.x / this.kinectPlayZoneWidth ) * this.displayPlayZoneWidth,
    //     (point.y / this.kinectPlayZoneHeight ) * this.displayPlayZoneHeight
    //   )
    // }

  // przeksztalca wspolrzedne punktu z obrazu kinecta na ten do wyswietlenia
  scalePosToView(point: Point){
    // "1 - " odwraca oś Y
    return new Point(
      // (point.x / this.kinectPlayZoneWidth ) * this.displayPlayZoneWidth,
      // ( 1 - (point.y / this.kinectPlayZoneHeight) ) * this.displayPlayZoneHeight
      ( 1 - (point.x / this.kinectPlayZoneWidth ) ) * this.displayPlayZoneWidth,
      ( (point.y / this.kinectPlayZoneHeight) ) * this.displayPlayZoneHeight
    )
  }

}
