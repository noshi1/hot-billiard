import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Ball } from 'src/app/models/ball';
import { Table } from 'src/app/models/table';
import { Pocket } from 'src/app/models/pocket';
import { TableService } from 'src/app/services/table.service';
import { Point } from 'src/app/models/point';
import { Training } from 'src/app/models/training/training';
import { Location } from '@angular/common'
import { NewPoint } from 'src/app/models/new-point';
import { PoolDisplayComponent } from '../pool-display/pool-display.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { TrainingService } from 'src/app/services/training.service';
import { InformationsService } from 'src/app/services/informations.service';
import { Informations } from 'src/app/models/informations';

@Component({
  selector: 'app-pool-page',
  templateUrl: './pool-page.component.html',
  styleUrls: ['./pool-page.component.css']
})
export class PoolPageComponent implements OnInit, OnDestroy {

  private unsubscribeSubject: Subject<void> = new Subject();

  @ViewChild('canvas', { static: true })
  private canvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;

  private bandSize: number = 62; // wynika z pikseli na obrazku (tło)
  private canvasHeight: number = 749; // wysokosc obrazka
  private canvasWidth: number = 1344; // szerokosc obrazka

  private kinectHeight: number;
  private kinectWidth: number;
  private displayHeight: number; // canvasHeight - (2 * bandSize)
  private displayWidth: number;  // canvasWidth - (2 * this.bandSize)

  // pozycje pocketow okreslajace miejsce sprawdzenia KLIKNIECIA pocketu
  touchPockets: Pocket[] = [
    new Pocket(0, new Point(0, 0)),
    new Pocket(1, new Point(this.canvasWidth / 2, 0)),
    new Pocket(2, new Point(this.canvasWidth, 0)),
    new Pocket(3, new Point(this.canvasWidth, this.canvasHeight)),
    new Pocket(4, new Point(this.canvasWidth / 2, this.canvasHeight)),
    new Pocket(5, new Point(0, this.canvasHeight))
  ];

  table: Table;
  training: Training;
  newPoints: NewPoint[] = [];
  private diameter: number;
  private touchPocketDiameter: number = 70;
  private pocketDiameter: number = 3;

  private hitPoints: Point[] = [];

  // View Mode:
  // 0 - pokazuje pozycje samych bil
  // 1 - tryb pasywny 1, rysuje jedna trajektorie
  // 2 - tryb pasywny 2, rysuje wszystkie mozliwe
  // 3 - tryb pasywny 3(pro), rysuje okrag styku bil
  // 10 - challenge
  private currentViewMode: number = 0;

  trainingStatus: String;
  information: Informations;

  constructor(private tableService: TableService, private location: Location,
    private trainingService: TrainingService,
    private informationsService: InformationsService) { }


  ngOnInit() {
    //this.fetchTable();

    var c = document.getElementById("canvas");
    c.addEventListener('click', (e) => {
      //console.log('canvas click');
      const mousePos = {
        x: e.clientX - c.offsetLeft,
        y: e.clientY - c.offsetTop
      };

      this.findClickedObjects(mousePos.x, mousePos.y);
    });

    this.ctx = this.canvas.nativeElement.getContext('2d');

    this.trainingService.fetchStatusLive()
      .pipe(takeUntil(this.unsubscribeSubject))
      .subscribe(trainingStatus => this.trainingStatus = trainingStatus);

    this.informationsService
      .fetchInformationsLive()
      .pipe(takeUntil(this.unsubscribeSubject))
      .subscribe(information => {
        this.information = information;
      });

    this.tableService
      .fetchTableLive()
      .pipe(takeUntil(this.unsubscribeSubject))
      .subscribe(table => {
        //console.clear();
        this.table = table;
        //console.log("live dziala");
        //console.log(table);
        //console.log(table.balls);

        this.kinectHeight = this.table.height;
        this.kinectWidth = this.table.width;
        this.displayHeight = this.canvasHeight - (2 * this.bandSize);
        this.displayWidth = this.canvasWidth - (2 * this.bandSize);
        this.diameter =
          ((this.displayHeight / this.kinectHeight)
            + (this.displayWidth / this.kinectWidth))
          / 2
          * 10;

        if (this.table.balls != null) {
          for (let ball of this.table.balls) {
            //console.log("scale ball");
            ball.point = this.scalePosToView(ball.point);
          }
        }

        if (this.table.pockets != null) {
          for (let pocket of this.table.pockets) {
            //console.log("scale pocket");
            pocket.point = this.scalePosToView(pocket.point);
          }
        }
        //console.log(this.table.balls);

        if (this.table.whiteBall != null){
          this.table.whiteBall.point = this.scalePosToView(this.table.whiteBall.point);
          //console.log("scale white");
        }
        if (this.table.selectedBall != null){
          this.table.selectedBall.point = this.scalePosToView(this.table.selectedBall.point);
         // console.log("scale sel bal");
        }
        if (this.table.selectedPocket != null){
          this.table.selectedPocket.point = this.scalePosToView(this.table.selectedPocket.point);
          //console.log("scale sel pock");
        }

        if (this.table.selectedChallenge == 0) {
          // rysuje tryby tylko jesli nie ma zaznaczonego challenge'u
          switch (this.table.selectedViewMode) {
            case 0: this.drawViewMode0(); break;
            case 1: this.drawViewMode1(); break;
            case 2: this.drawViewMode2(); break;
          }
        } else {
          // tryb challenge'u zostal wybrany.
          this.fetchTrainingById(this.table.selectedChallenge);
        }

        //this.drawErrorMessage("xd");
        //this.drawViewModeTitle("Tytulik");
      });
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  fetchTable() {
    this.tableService.fetchTable().then(response => {
      console.clear();
      console.log(response);
      this.table = response;

      this.kinectHeight = this.table.height;
      this.kinectWidth = this.table.width;
      this.displayHeight = this.canvasHeight - (2 * this.bandSize);
      this.displayWidth = this.canvasWidth - (2 * this.bandSize);
      this.diameter =
        ((this.displayHeight / this.kinectHeight)
          + (this.displayWidth / this.kinectWidth))
        / 2
        * 10;

      if (this.table.balls != null) {
        for (let ball of this.table.balls) {
          ball.point = this.scalePosToView(ball.point);
        }
      }

      if (this.table.pockets != null) {
        for (let pocket of this.table.pockets) {
          pocket.point = this.scalePosToView(pocket.point);
        }
      }

      if (this.table.whiteBall != null)
        this.table.whiteBall.point = this.scalePosToView(this.table.whiteBall.point);
      if (this.table.selectedBall != null)
        this.table.selectedBall.point = this.scalePosToView(this.table.selectedBall.point);
      if (this.table.selectedPocket != null)
        this.table.selectedPocket.point = this.scalePosToView(this.table.selectedPocket.point);

      console.log(this.table);

      if (this.table.selectedChallenge == 0) {
        // rysuje tryby tylko jesli nie ma zaznaczonego challenge'u
        switch (this.table.selectedViewMode) {
          case 0: this.drawViewMode0(); break;
          case 1: this.drawViewMode1(); break;
          case 2: this.drawViewMode2(); break;
        }
      } else {
        // tryb challenge'u zostal wybrany.
        this.fetchTrainingById(this.table.selectedChallenge);
      }
      //this.drawPockets();
      // this.drawBalls();
      // this.drawPockets();
      // this.fetchHitPoint();
      // this.fetchHints();
    }).catch(err => {
      console.log(err);
    });
  }




  drawViewMode0() {
    this.ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);
    this.drawViewModeTitle("Mode - Basic");
    this.drawBalls();
    this.drawWhiteBall();
    //this.drawPockets();
  }

  drawViewMode1() {
    //this.fetchTable();
    this.ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);
    this.drawViewModeTitle("Mode - Passive");
    this.drawBalls();
    this.drawWhiteBall();
    this.drawSelected();

    if ((this.table.selectedBall != null) && (this.table.selectedPocket != null)) {
      this.fetchHittingPoint();
    }
  }

  drawViewMode2() {
    this.ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);
    this.fetchHints();
  }

  drawPockets() {

    // rysuje prostokat wnetrza stolu - linie od luzy do luzy
    this.ctx.beginPath();
    this.ctx.moveTo(0, 0);
    this.ctx.lineTo(
      this.table.pockets[0].point.x + this.bandSize,
      this.table.pockets[0].point.y + this.bandSize
    );
    this.ctx.lineTo(
      this.table.pockets[2].point.x + this.bandSize,
      this.table.pockets[2].point.y + this.bandSize
    );
    this.ctx.lineTo(
      this.table.pockets[5].point.x + this.bandSize,
      this.table.pockets[5].point.y + this.bandSize
    );
    this.ctx.lineTo(
      this.table.pockets[3].point.x + this.bandSize,
      this.table.pockets[3].point.y + this.bandSize
    );
    this.ctx.lineTo(
      this.table.pockets[0].point.x + this.bandSize,
      this.table.pockets[0].point.y + this.bandSize
    );
    this.ctx.strokeStyle = "#FF0";
    this.ctx.stroke();

    //rysowanie obszaru "dotkniecie" pocketu
    for (let pocket of this.touchPockets) {
      this.ctx.beginPath();
      this.ctx.arc((pocket.point.x), (pocket.point.y), this.touchPocketDiameter, 0, 2 * Math.PI);
      this.ctx.fillStyle = "#FFF";
      this.ctx.fill();
    }

    // rysowanie punktu pocketu do ktorego jest wyznaczana trajektoria
    for (let pocket of this.table.pockets) {
      //console.log(pocket);
      this.ctx.beginPath();
      this.ctx.arc((pocket.point.x + this.bandSize), (pocket.point.y + this.bandSize), 10, 0, 2 * Math.PI);
      this.ctx.fillStyle = "#F00";
      this.ctx.fill();
    }
  }

  // rysuje obramowania wokol zaznaczonych elementow
  drawSelected() {
    if (this.table.selectedBall != null) {
      // rysowanie czerwonego okregu wokol zaznaczonej bili
      this.ctx.beginPath();
      this.ctx.arc(
        this.table.selectedBall.point.x + this.bandSize,
        this.table.selectedBall.point.y + this.bandSize,
        this.diameter + 5,
        0,
        2 * Math.PI
      );
      this.ctx.strokeStyle = "#F00";
      this.ctx.lineWidth = 3;
      this.ctx.stroke();
      this.ctx.lineWidth = 1;
    }

    if (this.table.selectedPocket != null) {
      //rysowanie obramowania zaznaczonego pocketu
      this.ctx.beginPath();
      this.ctx.arc(
        this.touchPockets[this.table.selectedPocket.id].point.x,
        this.touchPockets[this.table.selectedPocket.id].point.y,
        this.touchPocketDiameter + 5,
        0,
        2 * Math.PI);
      this.ctx.strokeStyle = "#F00";
      this.ctx.lineWidth = 10;
      this.ctx.stroke();
      this.ctx.lineWidth = 1;
    }
  }






  findClickedObjects(mouseXPos: number, mouseYPos: number) {
    if (this.currentViewMode == 1 || this.currentViewMode == 34 || this.currentViewMode == 36) {
      // wyszukuje tylko jesli jest aktywny tryb drugi, czyli tryb pasywny 1

      console.log("Wyszukiwanie: " + mouseXPos + ", " + mouseYPos);
      console.log(
        (((mouseXPos - this.bandSize) / this.displayWidth) * this.kinectWidth
        + ", " +
        (((mouseYPos - this.bandSize) / this.displayHeight) * this.kinectHeight
      )));


      for (let pocket of this.touchPockets) {
        // - banda size, dlatego ze klika na obrazek z bandami;
        if ((Math.pow(mouseXPos - pocket.point.x, 2)
          + Math.pow(mouseYPos - pocket.point.y, 2))
          < Math.pow(this.touchPocketDiameter, 2)
        ) {
          //console.log("Wykroto POCKET, ID: " + pocket.id);

          this.table.selectedPocket = this.table.pockets[pocket.id];
          this.selectPocket(pocket.id);
          return;
        }
      }

      for (let ball of this.table.balls) {
        if (
          // - banda size, dlatego ze klika na obrazek z bandami;
          (Math.pow(mouseXPos - this.bandSize - ball.point.x, 2) + Math.pow(mouseYPos - this.bandSize - ball.point.y, 2)) < Math.pow(this.diameter, 2)

        ) {
          //console.log("Wykroto bile, ID: " + ball.id);

          this.table.selectedBall = ball;
          this.selectBall(new Point((mouseXPos - this.bandSize) / this.displayWidth * this.kinectWidth,
            ((mouseYPos - this.bandSize) / this.displayHeight * this.kinectHeight
          )));
          return;
        }
      }
    }
  }

  drawTrajectory(hitPoints: Point[]) {


    if (hitPoints.length == 1) {
      // jeden punkt oznacza, ze jest prosta droga do łuzy

      // linia od bialej bili do wyznaczonej
      this.ctx.beginPath();
      this.ctx.moveTo(this.table.whiteBall.point.x + this.bandSize,
        this.table.whiteBall.point.y + this.bandSize
      );
      this.ctx.lineTo(this.hitPoints[0].x + this.bandSize,
        this.hitPoints[0].y + this.bandSize
      );

      // linia od wyznaczonej bili do łuzy
      this.ctx.lineTo(this.table.selectedPocket.point.x + this.bandSize, this.table.selectedPocket.point.y + this.bandSize);
      this.ctx.strokeStyle = "#FF0";
      this.ctx.stroke();
    }
    else if (hitPoints.length == 2) {
      // dwa punkty oznaczaja, ze najpierw jest odbicie od bandy [1] a pozniej do bili [0].

      // linia od bialej bili
      this.ctx.beginPath();
      this.ctx.moveTo(this.table.whiteBall.point.x + this.bandSize,
        this.table.whiteBall.point.y + this.bandSize
      );
      // do wyznaczonej bandy
      this.ctx.lineTo(this.hitPoints[1].x + this.bandSize,
        this.hitPoints[1].y + this.bandSize
      );
      // do wyznaczonej bili
      this.ctx.lineTo(this.hitPoints[0].x + this.bandSize,
        this.hitPoints[0].y + this.bandSize
      );

      // linia od wyznaczonej bili do łuzy
      this.ctx.lineTo(this.table.selectedPocket.point.x + this.bandSize, this.table.selectedPocket.point.y + this.bandSize);
      this.ctx.strokeStyle = "#FF0";
      this.ctx.stroke();
    } else { 
      // komunikat - nie mozna narysowac trajektorii.
    }

    // // rysowanie okregu bili w miejscu uderzenia
    // this.ctx.beginPath();
    //   this.ctx.arc(
    //     this.hitPoints[0].x + this.bandSize,
    //     this.hitPoints[0].y + this.bandSize,
    //     this.diameter,
    //     0,
    //     2 * Math.PI
    //   );
    //   this.ctx.strokeStyle = "#000";
    //   this.ctx.stroke();
  }

  // funkcja generuje losową wartość koloru do canvasa
  getRndColor() {
    var r = (255 * Math.random() + 0) | 0,
      g = (255 * Math.random() + 0) | 0,
      b = (255 * Math.random() + 0) | 0;
    return 'rgb(' + r + ',' + g + ',' + b + ')';
  }

  drawAllHints() {
    for (let punkt of this.newPoints) {
      // linia od bialej bili
      this.ctx.beginPath();
      this.ctx.moveTo(this.table.whiteBall.point.x + this.bandSize,
        this.table.whiteBall.point.y + this.bandSize
      );

      // do kazdej kolejnej bili
      this.ctx.lineTo(punkt.xBill + this.bandSize,
        punkt.yBill + this.bandSize
      );

      // do kazdej kolejnej luzy
      this.ctx.lineTo(punkt.xPocket + this.bandSize,
        punkt.yPocket + this.bandSize
      );
      this.ctx.strokeStyle = this.getRndColor();
      this.ctx.stroke();
    }
  }



  drawTraining() {
    this.ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);


    // rysowanie punktu ustawienia bialej bili
    this.ctx.beginPath();
    this.ctx.arc(
      this.training.whiteBallPosition.x + this.bandSize,
      this.training.whiteBallPosition.y + this.bandSize,
      this.diameter,
      0,
      2 * Math.PI
    );
    this.ctx.fillStyle = "#FFF";
    this.ctx.lineWidth = 3;
    this.ctx.fill();


    // rysowanie punktu ustawienia zaznaczonej bili
    this.ctx.beginPath();
    this.ctx.arc(
      this.training.selectedBallPosition.x + this.bandSize,
      this.training.selectedBallPosition.y + this.bandSize,
      this.diameter,
      0,
      2 * Math.PI
    );
    this.ctx.fillStyle = "#FF0";
    this.ctx.lineWidth = 3;
    this.ctx.fill();


    // rysowanie przeszkadzajek
    for (let ball in this.training.disturbBallsPositions) {
      this.ctx.beginPath();
      this.ctx.arc(
        this.training.disturbBallsPositions[ball].x + this.bandSize,
        this.training.disturbBallsPositions[ball].y + this.bandSize,
        this.diameter,
        0,
        2 * Math.PI
      );
      this.ctx.fillStyle = "#F00";
      this.ctx.lineWidth = 3;
      this.ctx.fill();
    }

    //rysowanie prostokatu w ktorym ma sie zatrzymac biala
    this.ctx.beginPath();
    this.ctx.rect(
      this.training.rectanglePosition[0].x + this.bandSize,
      this.training.rectanglePosition[0].y + this.bandSize,
      this.training.rectanglePosition[1].x - this.training.rectanglePosition[0].x,
      this.training.rectanglePosition[1].y - this.training.rectanglePosition[0].y
    );
    this.ctx.strokeStyle = "#FFF";
    this.ctx.stroke();

    // rysowanie zaznaczenia luzy do ktorej ma wpasc bila
    this.ctx.beginPath();
    this.ctx.arc(
      this.touchPockets[this.training.pocketId].point.x,
      this.touchPockets[this.training.pocketId].point.y,
      this.touchPocketDiameter + 5,
      0,
      2 * Math.PI);
    this.ctx.strokeStyle = "#FF0";
    this.ctx.lineWidth = 10;
    this.ctx.stroke();
    this.ctx.lineWidth = 1;
  }




  fetchHittingPoint() {
    this.tableService.fetchHittingPoint().then(response => {
      // reset pobranych punktow
      this.hitPoints = [];

      if (response[0] === undefined) {
        // nie zostaly wrocone w odpowiedzi zadne wspolrzedne. blad?
        console.log("fetchHittingPoint(); response[0] is unfefined");
        return;
      }
      else {
        // w odpowiedzi zostal poday pierwszy punkt
        response[0] = this.scalePosToView(response[0]);
        this.hitPoints.push(
          new Point(response[0].x, response[0].y)
        );
      }

      if (response[1] !== undefined) {
        // drugi punkt wspolrzednych istnieje.
        response[1] = this.scalePosToView(response[1]);
        this.hitPoints.push(
          new Point(response[1].x, response[1].y)
        );
      }

      this.drawTrajectory(this.hitPoints);
    }).catch(err => {
      this.drawSelected();
    });
  }

  fetchHints() {
    this.tableService.fetchHints().then(response => {
      this.newPoints = response;
      //console.log(this.newPoints);
      this.drawAllHints();
      this.drawBalls();
      //console.log(response.point);
      //this.newPoints = response;
      // console.log(response);
      // this.newPoints.push(new NewPoint(response[0].xBill,response[0].yBill,response[0].xPocket,response[0].yPocket));
      // console.log(this.newPoints);
      // for ( let punkt of response.){

      // }
      //this.newPoints = response;

      //this.hitPoint = response;
      //this.drawTrajectory(this.hitPoint);
      //this.ctx.clearRect(0,0,window.innerWidth,window.innerHeight);
    }).catch(err => {
      console.log(err);
    });
  }

  fetchTrainingById(id: number) {
    this.trainingService.fetchTrainingInPixelById(id).then ( training => {
      this.training = training;

      this.drawTraining();

    }).catch(err => {
      console.log(err);
    });
  }

  // selectBall(id: number) {
  //   this.tableService.selectBall(id).then(response => {
  //     // zaznaczenie bili zostalo zapisane w backendzie.
  //     this.drawViewMode1();
  //   }).catch(err => {
  //     console.log(err);
  //   });
  // }

  selectBall(point: Point) {
    //console.log(point);


    this.tableService.selectBall(point).then(response => {
      // zaznaczenie bili zostalo zapisane w backendzie.
      this.drawViewMode1();
    }).catch(err => {
      console.log(err);
    });
  }

  selectPocket(id: number) {
    this.tableService.selectPocket(id).then(response => {
      // zaznaczenie bili zostalo zapisane w backendzie.
      this.drawViewMode1();
    }).catch(err => {
      console.log(err);
    });
  }

  setViewMode(mode: number) {
    this.tableService.setViewMode(mode).then(response => {
      switch (mode) {
        case 0: {
          // tryb normalny
          this.currentViewMode = 0;
          this.drawViewMode0();
          break;
        }
        case 1: {
          // tryb pasywny 1
          this.currentViewMode = 1;
          this.drawViewMode1();
          break;
        }
        case 2: {
          // tryb pasywny 2
          this.currentViewMode = 2;
          this.drawViewMode2();
          break;
        }
        case 34: {
          // tryb pasywny 2
          this.currentViewMode = 34;
          this.drawViewMode1();
          break;
        }
        case 36: {
          // tryb pasywny 2
          this.currentViewMode = 36;
          this.drawViewMode1();
          break;
        }
        default: console.log("wrong change view mode parameter"); break;
      }
    }).catch(err => {
      console.log(err);
    });
  }

  setChallenge(id: number) {
    this.trainingService.setChallenge(id).then(response => {
      console.log("wybrany challenge:" + id);
    }).catch(err => {
      console.log(err);
    });
  }

  // przeksztalca wspolrzedne punktu z obrazu kinecta na ten do wyswietlenia
  scalePosToView(point: Point) {
    // "1 - " odwraca oś Y
    return new Point(
      (point.x / this.kinectWidth) * this.displayWidth,
      ( (point.y / this.kinectHeight)) * this.displayHeight
    )
  }

  navigateToPreviousPage() {
    this.location.back();
  }

  drawBall(ball: Ball, color: string) {
    this.ctx.beginPath();
    this.ctx.arc(
      ball.point.x + this.bandSize,
      ball.point.y + this.bandSize,
      this.diameter,
      0,
      2 * Math.PI
    );
    this.ctx.fillStyle = color;
    this.ctx.fill();
  }

  drawBalls() {
    if (this.table.balls == null)
      return;
    for (let ball of this.table.balls)
      this.drawBall(ball, "#000");
  }

  drawWhiteBall() {
    if (this.table.whiteBall == null)
      return;
    this.drawBall(this.table.whiteBall, "#FFF");
  }

  drawErrorMessage(errorMessage: string){
    this.ctx.beginPath();
    this.ctx.font = "30px Comic Sans MS";
    this.ctx.fillStyle = "red";
    this.ctx.textAlign = "center";
    this.ctx.fillText(errorMessage, this.canvasWidth/2, this.canvasHeight/2); 
  }

  drawViewModeTitle(title: string){
    this.ctx.beginPath();

    this.ctx.fillStyle = 'white';
    this.ctx.strokeStyle = 'black';
    
    this.ctx.font = '20pt Verdana';
    this.ctx.fillText(title, this.canvasWidth/8, this.bandSize/2);
    this.ctx.strokeText(title, this.canvasWidth/8, this.bandSize/2);
    
    this.ctx.fill();
    this.ctx.stroke();


    // this.ctx.font = "30px Comic Sans MS";
    // this.ctx.fillStyle = "red";
    // this.ctx.textAlign = "center";
    // this.ctx.fillText(title, this.canvasWidth/16, this.bandSize/2);
  }

  toggleHitInfo(){
    this.tableService.toggleHitInfo();
  }

}
