import { RectangleViewModel } from './rectangle-view-model';
import { Point } from 'src/app/models/point';

export class DragableRectangleViewModel {
  rectangle: RectangleViewModel = new RectangleViewModel();
  dragMode: DragMode = DragMode.None;
  
  move(newPos: Point) {
    switch (this.dragMode) {
      case DragMode.Move:
        this.rectangle.center = newPos;
        break;
      case DragMode.MoveRectMinX:
        this.setMinX(newPos);
        break;
      case DragMode.MoveRectMaxX:
        this.setMaxX(newPos);
        break;
      case DragMode.MoveRectMinY:
        this.setMinY(newPos);
        break;
      case DragMode.MoveRectMaxY:
        this.setMaxY(newPos);
        break;
      default:
        break;
    }
  }
  
  setMinX(newPos: Point) {
    if(newPos.x > this.rectangle.maxX) {
      this.rectangle.minX = this.rectangle.maxX;
      this.rectangle.maxX = newPos.x;
      this.dragMode = DragMode.MoveRectMaxX;
      return;
    }
  
    this.rectangle.minX = newPos.x;
  }
  
  setMinY(newPos: Point) {
    if(newPos.y > this.rectangle.maxY) {
      this.rectangle.minY = this.rectangle.maxY;
      this.rectangle.maxY = newPos.y;
      this.dragMode = DragMode.MoveRectMaxY;
      return;
    }
    this.rectangle.minY = newPos.y;
  }

  setMaxX(newPos: Point) {
    if(newPos.x < this.rectangle.minX) {
      this.rectangle.maxX = this.rectangle.minX;
      this.rectangle.minX = newPos.x;
      this.dragMode = DragMode.MoveRectMinX;
      return;
    }
    this.rectangle.maxX = newPos.x;
  }

  setMaxY(newPos: Point) {
    if(newPos.y < this.rectangle.minY) {
      this.rectangle.maxY = this.rectangle.minY;
      this.rectangle.minY = newPos.y;
      this.dragMode = DragMode.MoveRectMinY;
      return;
    }
    this.rectangle.maxY = newPos.y;
  }    
}

export enum DragMode {
  None,
  Move,
  MoveRectMinX,
  MoveRectMinY, 
  MoveRectMaxX,
  MoveRectMaxY
} 