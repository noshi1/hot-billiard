import { RectangleViewModel } from './rectangle-view-model';
import { Point } from 'src/app/models/point';

export class StatusBoxViewModel {
    rectangle: RectangleViewModel = new RectangleViewModel();
    rectangleOffset: Point = new Point(0, 0);
    statusText: string = "STATUS";
}