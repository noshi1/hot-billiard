import { Point } from "src/app/models/point";

export class RectangleViewModel {
  position: Point = new Point(0, 0);
  size: Point = new Point(20, 20);
  get center(): Point {
    return new Point(this.position.x + this.size.x / 2, 
      this.position.y + this.size.y / 2);
  }
  set center(value: Point) {
    this.position.x = value.x - this.size.x / 2;
    this.position.y = value.y - this.size.y / 2;
  }

  get min(): Point {
    return new Point(this.minX, this.minY);
  }
  set min(value: Point) {
    this.minX = value.x;
    this.minY = value.y;
  }

  get max(): Point {
    return new Point(this.maxX, this.maxY);
  }
  set max(value: Point) {
    this.maxX = value.x;
    this.maxY = value.y;
  }

  get minX(): number {
    return this.position.x;
  }
  set minX(value: number) {
    this.size.x += this.position.x - value;
    this.position.x = value;
  }

  get maxX(): number {
    return this.position.x + this.size.x;
  }
  set maxX(value: number) {
    this.size.x = value - this.position.x;
  }

  get minY(): number {
    return this.position.y;
  }
  set minY(value: number) {
    this.size.y += this.position.y - value;
    this.position.y = value;
  }

  get maxY(): number {
    return this.position.y + this.size.y;
  }
  set maxY(value: number) {
    this.size.y = value - this.position.y;
  }
}
    