import { Point } from 'src/app/models/point';
import { HitPoint } from 'src/app/models/training/hit-point.enum';

export class HitPointHintViewModel {
    position: Point;
    radius: number;
    hitPoint: HitPoint;

    constructor() {
        this.radius = 10;
        this.position = new Point(25, 25);
        this.hitPoint = HitPoint.Bottom;
    }
}

