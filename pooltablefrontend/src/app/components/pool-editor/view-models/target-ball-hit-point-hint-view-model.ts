import { Point } from 'src/app/models/point';

export class TargetBallHitPointHintViewModel {
    position: Point;
    radius: number;
    whiteBallOffsetPercent: number;

    constructor() {
        this.radius = 10;
        this.position = new Point(25, 25);
        this.whiteBallOffsetPercent = 50;
    }

    set center(center: Point) {
        this.position.x = center.x - (this.whiteBallOffsetPercent / 100) * this.radius;
        this.position.y = center.y;
    }

    get whiteBallOffset() {
        return new Point(this.whiteBallOffsetPercent / 100 * this.radius * 2, 0)
    }
}