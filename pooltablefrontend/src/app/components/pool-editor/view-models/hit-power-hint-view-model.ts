import { RectangleViewModel } from './rectangle-view-model';
import { Point } from 'src/app/models/point';

export class HitPowerHintViewModel {
    rectangle: RectangleViewModel;
    hitPower: number;

    constructor() {
        this.rectangle = new RectangleViewModel();
        this.rectangle.size.x = 10;
        this.rectangle.size.y = 20;
        this.hitPower = 50;
    }

    move(newPos: Point) {
        this.rectangle.center.x = newPos.x;
        this.rectangle.center.y = newPos.y;
    }
}