import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Point } from 'src/app/models/point';
import { Training } from 'src/app/models/training/training';
import { CalibrationParams } from 'src/app/models/calibration-params';
import { TableService } from 'src/app/services/table.service';
import { TrainingService } from 'src/app/services/training.service';
import { DifficultyLevel } from 'src/app/models/difficulty-level';
import { RectangleViewModel } from 'src/app/components/pool-editor/view-models/rectangle-view-model';
import { DragMode, DragableRectangleViewModel } from './view-models/dragable-rectangle-view-model';
import { ActivatedRoute } from '@angular/router';
import { HitPointHintViewModel } from './view-models/hit-point-hint-view-model';
import { HitPowerHintViewModel } from './view-models/hit-power-hint-view-model';
import { TargetBallHitPointHintViewModel } from './view-models/target-ball-hit-point-hint-view-model';
import { StatusBoxViewModel } from './view-models/status-box-view-model';
import { Location } from '@angular/common';
import { HitPoint } from 'src/app/models/training/hit-point.enum';
import { HitPointHint } from 'src/app/models/training/hit-point-hint';
import { HitPowerHint } from 'src/app/models/training/hit-power-hint';
import { TargetBallHitPointHint } from 'src/app/models/training/target-ball-hit-point-hint';

@Component({
  selector: 'app-pool-editor',
  templateUrl: './pool-editor.component.html',
  styleUrls: ['./pool-editor.component.css']
})
export class PoolEditorComponent implements OnInit {

  DragMode = DragMode;
  HitPoint = HitPoint;
  DifficultyLevel = DifficultyLevel;

  pocketsPos: Point[];
  training: Training;
  calibrationParams: CalibrationParams;

  defaultRectangleSize = new Point(20, 20);
  defaultHitPointHintRadius = 20;
  viewportSize = new Point(254, 127);
  ballRadius = 5.715 / 2;
  pocketDrawRadius = 8;

  rectangle: DragableRectangleViewModel;
  hitPointHint: HitPointHintViewModel;
  hitPowerHint: HitPowerHintViewModel;
  targetBallHitPointHint: TargetBallHitPointHintViewModel;
  statusBox: StatusBoxViewModel;

  selectedElement: any;
  selectedDragableElement: any;

  @ViewChild('svg', { static: false })
  svg: ElementRef;

  constructor(private tableService: TableService,
              private trainingService: TrainingService,
              private route: ActivatedRoute,
              private location: Location) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      var trainingId = params.get('trainingId');
      if(trainingId != null) {
        var trainingIdNumber = +trainingId; //as number
        this.trainingService.fetchTrainingById(trainingIdNumber).then(training => {
          this.loadDataFromTrainingInPercent(training);
        });
      }
    });

    this.training = new Training(null, null, [], [], null);
    this.training.difficultyLevel = DifficultyLevel.Normal;
    this.pocketsPos = [new Point(0, 0), new Point(this.viewportSize.x / 2, 0), new Point(this.viewportSize.x, 0),
      new Point(this.viewportSize.x, this.viewportSize.y), new Point(this.viewportSize.x / 2, this.viewportSize.y), new Point(0, this.viewportSize.y)];
    this.training.pocketId = 0;
  }

  selectPocket(index) {
    this.training.pocketId = index;
  }

  selectBall(ball: Point) {
    this.selectedElement = ball;
    this.selectedDragableElement = ball;
  }

  selectRectangle(rectangle: DragableRectangleViewModel, dragMode: DragMode) {
    rectangle.dragMode = dragMode;
    this.selectedElement = rectangle.rectangle;
    this.selectedDragableElement = rectangle;
  }

  selectHitPointHint(hitPointHint: HitPointHintViewModel) {
    this.selectedElement = hitPointHint;
    this.selectedDragableElement = hitPointHint;
  }

  selectHitPowerHint(hitPowerHint: HitPowerHintViewModel) {
    this.selectedElement = hitPowerHint;
    this.selectedDragableElement = hitPowerHint;
  }

  selectTargetBallHitPointHint(targetBallHitPointHint: TargetBallHitPointHintViewModel) {
    this.selectedElement = targetBallHitPointHint;
    this.selectedDragableElement = targetBallHitPointHint;
  }

  selectStatusBox(statusBox: StatusBoxViewModel) {
    this.selectedElement = statusBox;
    this.selectedDragableElement = statusBox;
  }

  moveSelectedElement(mouseMoveEvent: MouseEvent) {
    if(this.selectedDragableElement == null) {
      return;
    }
    var mousePos = this.getMousePosInCanvasViewport(mouseMoveEvent);

    if(this.selectedDragableElement instanceof Point) {
      this.moveBall(this.selectedDragableElement, mousePos);
    } else if(this.selectedDragableElement instanceof DragableRectangleViewModel) {
      this.moveRectangle(this.selectedDragableElement, mousePos);
    } else if(this.selectedDragableElement instanceof HitPointHintViewModel) {
      this.moveHitPointHint(this.selectedDragableElement, mousePos);
    } else if(this.selectedDragableElement instanceof HitPowerHintViewModel) {
      this.moveHitPowerHint(this.selectedDragableElement, mousePos);
    } else if(this.selectedDragableElement instanceof TargetBallHitPointHintViewModel) {
      this.moveTargetBallHitPointHint(this.selectedDragableElement, mousePos);
    } else if(this.selectedDragableElement instanceof StatusBoxViewModel) {
      this.moveStatusBox(this.selectedDragableElement, mousePos);
    }
  }
  
  disableDrag() {
    this.selectedDragableElement = null;
  }

  moveBall(ball: Point, newPos: Point) {
    ball.x = newPos.x;
    ball.y = newPos.y;
  }

  moveRectangle(rectangle: DragableRectangleViewModel, newPos: Point) {
    rectangle.move(newPos);
  }

  moveHitPointHint(hitPointHint: HitPointHintViewModel, newPos: Point) {
    hitPointHint.position.x = newPos.x;
    hitPointHint.position.y = newPos.y;
  }

  moveHitPowerHint(hitPowerHint: HitPowerHintViewModel, newPos: Point) {
    hitPowerHint.rectangle.center = new Point(newPos.x, newPos.y)
  }

  moveTargetBallHitPointHint(targetBallHitPointHint: TargetBallHitPointHintViewModel, newPos: Point) {
    targetBallHitPointHint.center = newPos;
  }

  moveStatusBox(statusBox: StatusBoxViewModel, newPos: Point) {
    statusBox.rectangle.center = new Point(newPos.x - statusBox.rectangleOffset.x, newPos.y - statusBox.rectangleOffset.y);
  }

  getMousePosInCanvasViewport(mouseEvent) {
    var pt = this.svg.nativeElement.createSVGPoint();
    pt.x = mouseEvent.clientX;
    pt.y = mouseEvent.clientY;

    return pt.matrixTransform(this.svg.nativeElement.getScreenCTM().inverse());
  }

  addWhiteBall() {
    if(this.training.whiteBallPosition == null) {
      this.training.whiteBallPosition = new Point(this.viewportSize.x / 2, this.viewportSize.y / 2);
    } else {
      this.training.whiteBallPosition.x = this.viewportSize.x / 2;
      this.training.whiteBallPosition.y = this.viewportSize.y / 2;
    }
  }

  addTargetBall() {
    if(this.training.selectedBallPosition == null) {
      this.training.selectedBallPosition = new Point(this.viewportSize.x / 2, this.viewportSize.y / 2);
    } else {
      this.training.selectedBallPosition.x = this.viewportSize.x / 2;
      this.training.selectedBallPosition.y = this.viewportSize.y / 2;
    }
  }

  addRectangle() {
    if(this.rectangle == null) {
      this.rectangle = new DragableRectangleViewModel();
    }
    this.rectangle.rectangle.size = new Point(this.defaultRectangleSize.x, this.defaultRectangleSize.y);
    this.rectangle.rectangle.center = new Point(this.viewportSize.x / 2, this.viewportSize.y / 2);
  }

  addDisturbBall() {
    this.training.disturbBallsPositions.push(new Point(this.viewportSize.x / 2, this.viewportSize.y / 2));
  }

  addHitPointHint() {
    if(this.hitPointHint == null) {
      this.hitPointHint = new HitPointHintViewModel();
    }

    this.hitPointHint.position = new Point(this.viewportSize.x / 2, this.viewportSize.y / 2);
    this.hitPointHint.radius = this.defaultHitPointHintRadius;
  }
  
  addHitPowerHint() {
    if(this.hitPowerHint == null) {
      this.hitPowerHint = new HitPowerHintViewModel();
    }

    this.hitPowerHint.rectangle.size = new Point(5, 50);
    this.hitPowerHint.rectangle.center = new Point(this.viewportSize.x / 2, this.viewportSize.y / 2);
  }

  addTargetBallHitPowerHint() {
    if(this.targetBallHitPointHint == null) {
      this.targetBallHitPointHint = new TargetBallHitPointHintViewModel();
    }

    this.targetBallHitPointHint.center = new Point(this.viewportSize.x / 2, this.viewportSize.y / 2);
    this.targetBallHitPointHint.radius = 10;
  }

  addStatusBox() {
    if(this.statusBox == null) {
      this.statusBox = new StatusBoxViewModel();
    }

    this.statusBox.rectangleOffset = new Point(-1, -10);
    this.statusBox.rectangle.size = new Point(29, 14);
    this.statusBox.rectangle.center = new Point(this.viewportSize.x / 2 - this.statusBox.rectangleOffset.x, this.viewportSize.y / 2 - this.statusBox.rectangleOffset.y);
  }

  removeSelectedElement() {
    if(this.isSelectedInstanceOfPoint()) {
      if(this.selectedElement === this.training.whiteBallPosition) {
        this.training.whiteBallPosition = null;
      } else if(this.selectedElement === this.training.selectedBallPosition) {
        this.training.selectedBallPosition = null;
      } else {
        var index = this.training.disturbBallsPositions.indexOf(this.selectedElement);
        if(index != -1) {
          this.training.disturbBallsPositions.splice(index, 1);
        }
      }
    } else if(this.isSelectedInstanceOfRectangle()) {
      this.rectangle = null;
    } else if(this.isSelectedInstanceOfHitPointHint()) {
      this.hitPointHint = null;
    } else if(this.isSelectedInstanceOfHitPowerHint()) {
      this.hitPowerHint = null;
    } else if(this.isSelectedInstanceOfTargetBallHitPowerHint()) {
      this.targetBallHitPointHint = null;
    } else if(this.isSelectedInstanceOfStatusBox()) {
      this.statusBox = null;
    }

    this.selectedElement = null;
    this.selectedDragableElement = null;
  }

  isSelectedInstanceOfPoint() {
    return this.selectedElement instanceof Point;
  }

  isSelectedInstanceOfRectangle() {
    return this.selectedElement instanceof RectangleViewModel;
  }

  isSelectedInstanceOfHitPointHint() {
    return this.selectedElement instanceof HitPointHintViewModel;
  }

  isSelectedInstanceOfHitPowerHint() {
    return this.selectedElement instanceof HitPowerHintViewModel;
  }

  isSelectedInstanceOfTargetBallHitPowerHint() {
    return this.selectedElement instanceof TargetBallHitPointHintViewModel;
  }

  isSelectedInstanceOfStatusBox() {
    return this.selectedElement instanceof StatusBoxViewModel;
  }

  clearTraining() {
    this.training.whiteBallPosition = null;
    this.training.selectedBallPosition = null;
    this.training.disturbBallsPositions = []
    this.training.rectanglePosition = [];
    this.training.pocketId = 0;

    this.rectangle = null;
    this.selectedElement = null;
    this.selectedDragableElement = null;
  }

  fetchFromTable() {
    this.tableService.fetchTable().then(table => {
      this.training.whiteBallPosition = null;
      this.training.selectedBallPosition = null;
      this.training.disturbBallsPositions = []
      this.training.rectanglePosition = [];

      var tableSize: Point = new Point(table.width, table.height);
      if(table.whiteBall != null) {
        this.training.whiteBallPosition = this.convertPointFromPixelToViewport(table.whiteBall.point, tableSize);
      }

      if(table.balls != null && table.balls.length != 0) {
        this.training.selectedBallPosition = this.convertPointFromPixelToViewport(table.balls[0].point, tableSize);

        this.training.disturbBallsPositions = table.balls.map(ball => this.convertPointFromPixelToViewport(ball.point, tableSize));
        this.training.disturbBallsPositions.shift();
      }
    });
  }

  save() {
    var training = this.convertTrainingToTrainingInPercent();
    this.trainingService.save(training).then(trainingResponse => {
      this.loadDataFromTrainingInPercent(trainingResponse);
      this.selectedElement = null;
      this.selectedDragableElement = null;
    });
  }

  canSave() {
    if(this.training.whiteBallPosition != null && this.training.selectedBallPosition != null && this.rectangle != null && this.statusBox != null) {
      return true;
    }
    return false;
  }

  loadDataFromTrainingInPercent(training: Training) {
    this.training.id = training.id;
    this.training.difficultyLevel = training.difficultyLevel;
    this.training.whiteBallPosition = this.convertPointFromPercentToViewport(training.whiteBallPosition);
    this.training.selectedBallPosition = this.convertPointFromPercentToViewport(training.selectedBallPosition);
    this.training.disturbBallsPositions = training.disturbBallsPositions.map(point => this.convertPointFromPercentToViewport(point));
    this.training.rectanglePosition = training.rectanglePosition.map(point => this.convertPointFromPercentToViewport(point));
    this.training.pocketId = training.pocketId;

    this.rectangle = new DragableRectangleViewModel();
    this.rectangle.rectangle.min = this.training.rectanglePosition[0];
    this.rectangle.rectangle.max = this.training.rectanglePosition[1];

    this.addStatusBox();
    this.statusBox.rectangle.position = this.convertPointFromPercentToViewport(training.statusPosition);

    if(training.hitPointHint != null) {
      this.addHitPointHint();
      this.hitPointHint.position = this.convertPointFromPercentToViewport(training.hitPointHint.position);
      this.hitPointHint.radius = this.convertNumberFromPercentToViewport(training.hitPointHint.radius);
      this.hitPointHint.hitPoint = training.hitPointHint.hitPoint;
    }

    if(training.hitPowerHint != null) {
      this.addHitPowerHint();
      this.hitPowerHint.rectangle.size = this.convertPointFromPercentToViewport(training.hitPowerHint.size);
      this.hitPowerHint.rectangle.position = this.convertPointFromPercentToViewport(training.hitPowerHint.position);
      this.hitPowerHint.hitPower = training.hitPowerHint.hitPower;
    }

    if(training.targetBallHitPointHint != null) {
      this.addTargetBallHitPowerHint();
      this.targetBallHitPointHint.position = this.convertPointFromPercentToViewport(training.targetBallHitPointHint.targetBall);
      this.targetBallHitPointHint.radius = this.convertNumberFromPercentToViewport(training.targetBallHitPointHint.radius);
      this.targetBallHitPointHint.whiteBallOffsetPercent = (this.convertPointFromPercentToViewport(training.targetBallHitPointHint.whiteBall).x - this.targetBallHitPointHint.position.x) / (2 * this.targetBallHitPointHint.radius) * 100 as number;
    }
  }

  convertTrainingToTrainingInPercent() {
    var training = new Training(this.convertPointFromViewportToPercert(this.training.whiteBallPosition),
      this.convertPointFromViewportToPercert(this.training.selectedBallPosition),
      this.training.disturbBallsPositions.map(point => this.convertPointFromViewportToPercert(point)),
      [this.convertPointFromViewportToPercert(this.rectangle.rectangle.min), this.convertPointFromViewportToPercert(this.rectangle.rectangle.max)],
      this.training.pocketId);
    training.difficultyLevel = this.training.difficultyLevel;
    training.id = this.training.id;
    training.statusPosition = this.convertPointFromViewportToPercert(this.statusBox.rectangle.position);

    if(this.hitPointHint != null) {
      var hitPointHint = new HitPointHint();
      hitPointHint.position = this.convertPointFromViewportToPercert(this.hitPointHint.position);
      hitPointHint.radius = this.convertNumberFromViewportToPercert(this.hitPointHint.radius); 
      hitPointHint.hitPoint = this.hitPointHint.hitPoint;

      training.hitPointHint = hitPointHint;
    }

    if(this.hitPowerHint != null) {
      var hitPowerHint = new HitPowerHint();
      hitPowerHint.position = this.convertPointFromViewportToPercert(this.hitPowerHint.rectangle.position);
      hitPowerHint.size = this.convertPointFromViewportToPercert(this.hitPowerHint.rectangle.size);
      hitPowerHint.hitPower = this.hitPowerHint.hitPower;

      training.hitPowerHint = hitPowerHint;
    }

    if(this.targetBallHitPointHint != null) {
      var targetBallHitPointHint = new TargetBallHitPointHint();
      targetBallHitPointHint.targetBall = this.convertPointFromViewportToPercert(this.targetBallHitPointHint.position);
      targetBallHitPointHint.whiteBall = this.convertPointFromViewportToPercert(new Point(this.targetBallHitPointHint.position.x + this.targetBallHitPointHint.whiteBallOffset.x, this.targetBallHitPointHint.position.y));
      targetBallHitPointHint.radius = this.convertNumberFromViewportToPercert(this.targetBallHitPointHint.radius);

      training.targetBallHitPointHint = targetBallHitPointHint;
    }

    return training;
  }  

  convertPointFromPixelToViewport(point: Point, tableSize: Point) {
    return new Point((point.x / tableSize.x) * this.viewportSize.x, (point.y / tableSize.y) * this.viewportSize.y);
  }

  convertPointFromViewportToPercert(point: Point) {
    return new Point(point.x / this.viewportSize.x, point.y / this.viewportSize.y);
  }

  convertPointFromPercentToViewport(point: Point) {
    return new Point(point.x * this.viewportSize.x, point.y * this.viewportSize.y);
  }

  convertNumberFromViewportToPercert(number: number) { // backend should also use x
    return number / this.viewportSize.x;
  }

  convertNumberFromPercentToViewport(number: number) { // backend should also use x
    return number * this.viewportSize.x;
  }

  navigateToPreviousPage() {
    this.location.back();
  }
}
