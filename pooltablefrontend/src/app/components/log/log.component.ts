import { Component, OnInit } from '@angular/core';
import { LogService } from 'src/app/services/log.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {

  private unsubscribeSubject: Subject<void> = new Subject();
  
  constructor(private logService: LogService) { }

  ngOnInit() {
    this.logService.onLogMessage()
      .pipe(takeUntil(this.unsubscribeSubject))
      .subscribe(msg => console.log(msg));
  }
}
