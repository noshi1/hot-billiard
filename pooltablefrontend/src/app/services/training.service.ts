import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Training } from '../models/training/training';
import { DifficultyLevel } from '../models/difficulty-level';
import { SocketClientService } from './socket-client.service';
import { HitPoint } from '../models/training/hit-point.enum';
import { TrainingModeParams } from '../models/training/training-mode-params';
import { map } from 'rxjs/operators';
import { AfterEndAction } from '../models/training/after-end-action.enum';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {

  constructor(private HttpClient: HttpClient,
              private socketClientService: SocketClientService) { }

  fetchTrainingById(id: number) {
    return this.HttpClient.get(
      `${environment.restApi}/training/getById/${id}`
    ).toPromise().then(response => this.mapResponseToModel(response));
  }

  fetchTrainingInPixelById(id: number) {
    return this.HttpClient.get(
      `${environment.restApi}/training/getInPixelById/${id}`
    ).toPromise().then(response => this.mapResponseToModel(response));
  }

  fetchTrainingsIdsByDifficultyLevel(difficultyLevel: DifficultyLevel) {
    return this.HttpClient.get(
      `${environment.restApi}/training/getIdsByDifficultyLevel/${difficultyLevel}`
    ).toPromise().then(response => response as number[]);
  }

  fetchTrainingsByDifficultyLevel(difficultyLevel: DifficultyLevel, page: number, size: number) {
    return this.HttpClient.get(
      `${environment.restApi}/training/getByDifficultyLevel/${difficultyLevel}?page=${page}&size=${size}`
    ).toPromise();
  }

  fetchStatusLive() {
    return this.socketClientService
      .onPlainMessage("/training/state");
  }
  
  setChallenge(id: number) {
    return this.HttpClient.put(
      `${environment.restApi}/training/setChallenge/${id}`, null
    ).toPromise();
  }

  save(training: Training) {
    if(training.id == null) {
      return this.insert(training);
    } else {
      return this.update(training);
    }
  }

  deleteTrainingById(trainingId: number) {
    return this.HttpClient.delete(
      `${environment.restApi}/training/delete/${trainingId}`
    ).toPromise().then(response => this.mapResponseToModel(response));
  }

  fetchTrainingModeParams() {
    return this.HttpClient.get(
      `${environment.restApi}/training/params/get`
    ).toPromise().then(response => this.mapTrainingModeParamsResponseToModel(response));
  }

  updateTrainingModeParams(trainingModeParams: TrainingModeParams) {
    return this.HttpClient.put(
      `${environment.restApi}/training/params/update`, trainingModeParams
    ).toPromise().then(response => this.mapTrainingModeParamsResponseToModel(response));
  }

  private insert(training: Training) {
    return this.HttpClient.post(
      `${environment.restApi}/training/insert`, training
    ).toPromise().then(response => this.mapResponseToModel(response));
  }
  private update(training: Training) {
    return this.HttpClient.put(
      `${environment.restApi}/training/update`, training
    ).toPromise().then(response => this.mapResponseToModel(response));
  }

  private mapResponseToModel(response: Object): Training {
    var training = response as Training;
    training.difficultyLevel = DifficultyLevel[response["difficultyLevel"] as string];
    if(training.hitPointHint != null) {
      training.hitPointHint.hitPoint = HitPoint[response["hitPointHint"]['hitPoint'] as string];
    }
    return training;
  }

  private mapTrainingModeParamsResponseToModel(response: Object): TrainingModeParams {
    var trainingModeParams = response as TrainingModeParams;
    trainingModeParams.afterSuccessAction = AfterEndAction[response["afterSuccessAction"] as string];
    trainingModeParams.afterFailAction = AfterEndAction[response["afterFailAction"] as string];
    
    return trainingModeParams;
  }
}
