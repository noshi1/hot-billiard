import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SocketClientService } from './socket-client.service';
import { CalibrationParams } from '../models/calibration-params';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CalibrationService {

  constructor(private httpClient: HttpClient,
              private socketClientService: SocketClientService) { }

  fetchCalibration() {
    return this.httpClient
      .get(`${environment.restApi}/calibration`)
      .toPromise()
      .then(calibration => calibration as CalibrationParams);
  }

  fetchCalibrationLive() {
    return this.socketClientService
      .onMessage("/calibration/live")
      .pipe(map(calibration => calibration as CalibrationParams));
  }

  updateCalibration(calibration: CalibrationParams) {
    return this.httpClient.put(`${environment.restApi}/calibration`, calibration).toPromise();
  }

  autoCalibration() {
    return this.httpClient.get(`${environment.restApi}/calibration/automatic`).toPromise();
  }

  resetToDefault() {
    return this.httpClient
      .get(`${environment.restApi}/calibration/reset`)
      .toPromise()
      .then(calibration => calibration as CalibrationParams);
  }
}
