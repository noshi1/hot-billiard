import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PoolDrawerParams } from '../models/pool-drawer-params';

@Injectable({
  providedIn: 'root'
})
export class PoolDrawerParamsService {

  constructor(private HttpClient: HttpClient) { }

  fetch() {
    return this.HttpClient.get(
      `${environment.restApi}/poolDrawerParams/get`
    ).toPromise().then(response => response as PoolDrawerParams);
  }

  update(poolDrawerParams: PoolDrawerParams) {
    return this.HttpClient.put(
      `${environment.restApi}/poolDrawerParams/update`, poolDrawerParams
    ).toPromise().then(response => response as PoolDrawerParams);
  }
}
