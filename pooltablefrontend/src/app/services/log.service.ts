import { Injectable, OnInit } from '@angular/core';
import { SocketClientService } from './socket-client.service';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private socketClientService: SocketClientService) { }

  onLogMessage() {
    return this.socketClientService.onPlainMessage("/log");
  }
}
