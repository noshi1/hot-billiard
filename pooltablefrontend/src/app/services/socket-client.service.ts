import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, first, switchMap } from 'rxjs/operators';
import { Client, StompSubscription, Message } from '@stomp/stompjs';
import { SocketClientState } from '../models/socket-client-state.enum';
import { environment } from 'src/environments/environment';

// https://g00glen00b.be/websockets-angular/
// https://stomp-js.github.io/guide/stompjs/2018/09/08/upgrading-stompjs.html
@Injectable({
  providedIn: 'root'
})
export class SocketClientService implements OnDestroy {

  private client: Client;
  private state: BehaviorSubject<SocketClientState>;

  constructor() {
    this.state = new BehaviorSubject<SocketClientState>(SocketClientState.ATTEMPTING);
    this.client = new Client({
      brokerURL: environment.webSocketApi,
      reconnectDelay: 5000,
      heartbeatIncoming: 4000,
      heartbeatOutgoing: 4000
    });

    this.client.onConnect = (frame) => this.state.next(SocketClientState.CONNECTED);

    this.client.onStompError = (frame) => {
      
      console.log('Broker reported error: ' + frame.headers['message']);
      console.log('Additional details: ' + frame.body);
    };

    this.client.activate();
  }

  private connect(): Observable<Client> {
    var obs = new Observable<Client>(observer => {
      this.state.pipe(filter(state => state === SocketClientState.CONNECTED)).subscribe(() => {
        observer.next(this.client);
      });
    });

    return obs;
  }

  ngOnDestroy(): void {
    this.connect().pipe(first()).subscribe(client => client.deactivate());
  }

  onMessage(topic: string, handler = SocketClientService.jsonHandler): Observable<any> {
    return this.connect().pipe(first(), switchMap(client => {
      return new Observable<any>(observer => {
        const subscription: StompSubscription = client.subscribe(topic, message => {
          observer.next(handler(message));
        });
        return () => client.unsubscribe(subscription.id);
      });
    }));
  }

  onPlainMessage(topic: string): Observable<string> {
    return this.onMessage(topic, SocketClientService.textHandler);
  }
  
  send(topic: string, payload: any): void {
    this.connect()
      .pipe(first())
      .subscribe(client => client.publish({destination: topic, body: JSON.stringify(payload)}));
  }

  static jsonHandler(message: Message): any {
    return JSON.parse(message.body);
  }
  
  static textHandler(message: Message): string {
    return message.body;
  }
}
