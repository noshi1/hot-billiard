import { Injectable } from '@angular/core';
import { SocketClientService } from './socket-client.service';
import { Informations } from '../models/informations';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InformationsService {

  constructor(private socketClientService: SocketClientService) { }

  fetchInformationsLive() {
    return this.socketClientService
      .onMessage("/informations")
      .pipe(map(informations => informations as Informations));
  }
}
