import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Table } from '../models/table';
import { Point } from '../models/point';
import { NewPoint } from '../models/new-point';
import { Pocket } from '../models/pocket';
import { Observable } from 'rxjs';
import { SocketClientService } from './socket-client.service';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor(private HttpClient: HttpClient,
              private socketClientService: SocketClientService) { }

  fetchTable() {
    return this.HttpClient.get(
      `${environment.restApi}/table`
    ).toPromise().then(response => response as Table);
  }

  fetchTableLive() : Observable<Table> {
    return this.socketClientService
      .onMessage("/table/live")
      .pipe(map(obj => obj as Table))
  }


  drawTableLive(): Observable<String> {
    console.log("draw table live");
    //console.log("this.socketClientService.onMessage(\"/table/draw\")");
    //console.log(this.socketClientService.onMessage("/table/draw"));
    return this.socketClientService
      .onPlainMessage("/table/draw")
      // .pipe(map(image => image as Blob))
  }

  fetchHittingPoint() {
    return this.HttpClient.put(
      `${environment.restApi}/table/hit`, null
    ).toPromise().then(response => response as Point[]);
  }

  fetchHints() {
    return this.HttpClient.put(
      `${environment.restApi}/table/hints`, null
    ).toPromise().then(response => response as NewPoint[]);
  }

  selectBall(point: Point) {
    return this.HttpClient.put(
      `${environment.restApi}/table/ball`, point
    ).toPromise().then(response => response as Point);
  }

  selectPocket(id: number) {
    return this.HttpClient.put(
      `${environment.restApi}/table/pocket/${id}`, null
    ).toPromise().then(response => response as Pocket);
  }

  setViewMode(id: number) {
    return this.HttpClient.put(
      `${environment.restApi}/table/setViewMode/${id}`, null
    ).toPromise();
  }

  toggleHitInfo() {
    return this.HttpClient.put(
      `${environment.restApi}/table/toggleHitInfo`, null
    ).toPromise();
  }
}
