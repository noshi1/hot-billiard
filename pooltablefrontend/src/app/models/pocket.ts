import { Point } from './point';

export class Pocket {
    id: number;
    point: Point;


    constructor(id : number, point: Point) {
        this.id = id;
        this.point = point;
    }
}
