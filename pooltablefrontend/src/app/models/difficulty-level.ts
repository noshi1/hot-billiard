export enum DifficultyLevel {
    Easy = "Easy",
    Normal = "Normal",
    Hard = "Hard"
}
