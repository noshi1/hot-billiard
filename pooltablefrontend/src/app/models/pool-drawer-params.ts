export class PoolDrawerParams {

    lineThickness: number = 2;
    ballRadius: number = 20;
    ballColor: string;
    whiteBallRadius: number = 20;
    whiteBallColor: string;
    pocketColor: string;

    selectedBallRadius: number = 20;
    selectedBallColor: string;
    selectedPocketColor: string;

    trainingSelectedBallColor: string;
    disturbBallColor: string;
    
    trajectoryLineThickness: number;
    trajectoryLineColor: string;
    followRotationLineColor: string;
    zeroRotationLineColor: string;
    hiddenPlacesColor: string;
}
