import { Point } from '../point';
import { HitPoint } from './hit-point.enum';

export class HitPointHint {
    position: Point;
    radius: number;
    hitPoint: HitPoint;
}
