export enum AfterEndAction {
    SameLevel,
    NextLevel,
    RandomLevelOfSameDifficulty,
    RandomLevelOfAnyDifficulty,
}
