import { Point } from '../point';
import { DifficultyLevel } from '../difficulty-level';
import { HitPowerHint } from './hit-power-hint';
import { HitPointHint } from './hit-point-hint';
import { TargetBallHitPointHint } from './target-ball-hit-point-hint';

export class Training {
    id: number;
    difficultyLevel: DifficultyLevel;
    whiteBallPosition: Point;
    selectedBallPosition: Point;
    disturbBallsPositions: Point[];
    rectanglePosition: Point[];
    pocketId: number;
    statusPosition: Point;
    hitPointHint: HitPointHint;
    hitPowerHint: HitPowerHint;
    targetBallHitPointHint: TargetBallHitPointHint;
    imagePreview: String;
    

    constructor(
        whiteBallPosition: Point,
        selectedBallPosition: Point,
        disturbBallsPositions: Point[],
        rectanglePosition: Point[],
        pocketId: number
    ) {
        this.whiteBallPosition = whiteBallPosition;
        this.selectedBallPosition = selectedBallPosition;
        this.disturbBallsPositions = disturbBallsPositions;
        this.rectanglePosition = rectanglePosition;
        this.pocketId = pocketId;
    }
}
