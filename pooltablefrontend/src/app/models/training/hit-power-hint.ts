import { Point } from '../point';

export class HitPowerHint {
    position: Point;
    size: Point;
    hitPower: number;
}
