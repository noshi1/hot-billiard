export enum HitPoint {
    Middle,
    Top,
    RightTop,
    Right,
    RightBottom,
    Bottom,
    LeftBottom,
    Left,
    LeftTop
}