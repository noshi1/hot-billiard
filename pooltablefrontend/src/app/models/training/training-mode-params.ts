import { AfterEndAction } from './after-end-action.enum';

export class TrainingModeParams {
    waitingForBallsPlacementDelay: number;
    ballsStopMovingDelay: number;
    afterEndDelay: number;
    ballPositionTolerance: number;

    afterSuccessAction: AfterEndAction;
    afterFailAction: AfterEndAction;
}



