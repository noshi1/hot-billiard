import { Point } from '../point';

export class TargetBallHitPointHint {
    whiteBall: Point;
    targetBall: Point;
    radius: number;
}
