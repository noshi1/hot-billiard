import { Point } from './point';

export class CalibrationParams {
    leftBottomCorner: Point = new Point(0, 0);
    leftUpperCorner: Point = new Point(0, 10);
    rightBottomCorner: Point = new Point(10, 0);
    rightUpperCorner: Point = new Point(10, 10);
    ballDiameter: number = 20;
    tableSizeInCm: Point = new Point(254, 127);
    whiteBallDensity: number = 375000;
    leftBottomCornerProjector: Point = new Point(0, 0);
    leftUpperCornerProjector: Point = new Point(0, 10);
    rightBottomCornerProjector: Point = new Point(10, 0);
    rightUpperCornerProjector: Point = new Point(10, 10);
}
