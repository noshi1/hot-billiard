export class ViewModeFeatures {
    hiddenPlaces: boolean = false;
    rotation: boolean = false;
    noRotation: boolean = false;
    bestPocket: boolean = false;
}
