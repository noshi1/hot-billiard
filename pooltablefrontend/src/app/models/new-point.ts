export class NewPoint {
    xBill: number;
    yBill: number;
    xPocket: number;
    yPocket: number;


    constructor(xBill : number, yBill : number, xPocket: number, yPocket: number) {
        this.xBill = xBill;
        this.yBill = yBill;
        this.xPocket = xPocket;
        this.yPocket = yPocket;
    }
}
