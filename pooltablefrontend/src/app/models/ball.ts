import { Point } from './point';

export class Ball {
    id: number;
    diameter: number;
    selected: boolean;
    point: Point;
    white: boolean;


    constructor(id: number, point: Point){
        this.id = id;
        this.point = point;
    }
}

