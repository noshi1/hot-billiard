import { PassiveModeLevel } from './passive-mode-level';


// temporally, later will use viewMode and viewModeFeatures
export class ViewModeParams {
    passiveModeLevel: PassiveModeLevel = PassiveModeLevel.Normal;
    isShowDistanceToBallActive: boolean = true;
    isShowDistanceToPocketActive: boolean = true;
    isShowAngleActive: boolean = true;
}
