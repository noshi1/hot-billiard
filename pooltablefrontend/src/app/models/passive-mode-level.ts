export enum PassiveModeLevel {
    Off = 1,
    Normal = 2,
    Pro = 3
}