import { Ball } from './ball';
import { Pocket } from './pocket';
import { Point } from './point';

export class Table {
    height: number;
    width: number;
    balls: Ball[];
    pockets: Pocket[];
    whiteBall: Ball;
    selectedBall: Ball;
    selectedPocket: Pocket;
    hittingPoint: Point;
    allPossibleHits: Point[];
    selectedViewMode: number;
    selectedChallenge: number;
    


    constructor(
        height: number,
        width: number,
        balls: Ball[],
        pockets: Pocket[],
        whiteBall: Ball,
        selectedBall: Ball,
        selectedPocket: Pocket,
        hittingPoint: Point,
        allPossibleHits: Point[],
        selectedViewMode: number,
        selectedChallenge: number
    ) {
        this.height = height;
        this.width = width;
        this.balls = balls;
        this.pockets = pockets;
        this.whiteBall = whiteBall;
        this.selectedBall = selectedBall;
        this.selectedPocket = selectedPocket;
        this.hittingPoint = hittingPoint;
        this.allPossibleHits = allPossibleHits;
        this.selectedViewMode = selectedViewMode;
        this.selectedChallenge = selectedChallenge;
    }
}
