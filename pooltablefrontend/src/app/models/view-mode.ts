export class ViewMode {
    basic:      number = 0;
    passive:    number = 1;
    passivePro: number = 2;
    training:   number = 3;
}