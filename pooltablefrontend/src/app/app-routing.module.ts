import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainMenuComponent } from './components/menu/main/main-menu.component';
import { OptionsComponent } from './components/menu/options/options.component';
import { NewGameComponent } from './components/menu/new-game/new-game.component';
import { ChallengeDifficultyComponent } from './components/menu/new-game/challenge/challenge-difficulty/challenge-difficulty.component';
import { ChallengeLevelComponent } from './components/menu/new-game/challenge/challenge-level/challenge-level.component';
import { PoolPageComponent } from './components/pool-page/pool-page.component';
import { PoolDisplayComponent } from './components/pool-display/pool-display.component';
import { ImgLiveComponent } from './components/img-live/img-live.component';
import { PoolEditorComponent } from './components/pool-editor/pool-editor.component';
import { LogComponent } from './components/log/log.component';
import { TrainingModeParamsComponent } from './components/menu/options/training-mode-params/training-mode-params.component';
import { PoolDrawerParamsComponent } from './components/menu/options/pool-drawer-params/pool-drawer-params.component';
import { CalibrationParamsComponent } from './components/menu/options/calibration-params/calibration-params.component';
import { ViewModeParamsComponent } from './components/menu/options/view-mode-params/view-mode-params.component';



const routes: Routes = [
  {
    path:'poolTable',
    component: PoolPageComponent
  },
  {
    path:'display',
    component: PoolDisplayComponent
  },
  {
    path:'imglive',
    component: ImgLiveComponent
  },
  {
    path:'poolEditor',
    component: PoolEditorComponent
  },
  {
    path:'poolEditor/:trainingId',
    component: PoolEditorComponent
  },
  {
    path:'log',
    component: LogComponent
  },
  {
    path: '',
    redirectTo: '/mainMenu',
    pathMatch: 'full'
  },
  {
    path: 'mainMenu',
    component: MainMenuComponent
  },
  {
    path: 'newGame',
    component: NewGameComponent
  },
  {
    path: 'options',
    component: OptionsComponent
  },
  {
    path: 'options/calibrationParams',
    component: CalibrationParamsComponent
  },
  {
    path: 'options/viewModeParams',
    component: ViewModeParamsComponent
  },
  {
    path: 'options/trainingModeParams',
    component: TrainingModeParamsComponent
  },
  {
    path: 'options/poolDrawerParams',
    component: PoolDrawerParamsComponent
  },
  {
    path: 'challenge/difficulty',
    component: ChallengeDifficultyComponent
  },
  {
    path: 'challenge/level/:difficultyLevel',
    component: ChallengeLevelComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

